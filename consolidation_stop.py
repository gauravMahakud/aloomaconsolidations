#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 11 15:05:07 2019

@author: gaurav mahakud
"""

import requests
import json
import datetime

from alooma import Client

USERNAME = '--' #add user name string
PASSWORD = '--' #add password as string
EVENT_TYPE = 'Quickr_data'

api = Client(username=USERNAME, password=PASSWORD)

mapping = api.get_mapping(EVENT_TYPE)

mapping['consolidation'] = None
api.set_mapping(mapping, EVENT_TYPE)