#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 11 15:10:00 2019

@author: gaurav Mahakud
"""
import os
import requests
from alooma import Client

class endpoints:

   # Consolidation V2
   CONSOLIDATION_V2 = 'v2/consolidation'
   CONSOLIDATION_STATE_V2 = 'v2/consolidation/{query_id}'
   CONSOLIDATION_RUN_V2 = 'v2/consolidation/{query_id}/run'

class ExtClient(Client):

   def delete_consolidation(self, query_id):
       " Delete Consolidation "
       url = self.rest_url + endpoints.CONSOLIDATION_STATE_V2.format(
                                       query_id=query_id)
       res = self._Client__send_request(requests.delete, url)

       return res.ok


ALOOMA_USERNAME = '--' #add you email as string
ALOOMA_PASSWORD = '--' #add your password as string
ACCOUNT_NAME = 'jumbotail'
EVENT_TYPE = 'Quickr_data'

api = ExtClient(ALOOMA_USERNAME, ALOOMA_PASSWORD, account_name=ACCOUNT_NAME)
queries = api.get_scheduled_queries()

for query in queries.values():
   if query['configuration']['event_type'] == EVENT_TYPE:
       break

query_id = query['state_id']

api.delete_consolidation(query_id)