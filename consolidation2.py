#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 13:13:23 2019

@author: gaurav Mahakud
"""

import requests
from alooma import Client
CONSOLIDATION_ENDPOINT = 'consolidation'
class ExtClient(Client):

   def create_consolidation(self, configuration):

       url = self.rest_url + CONSOLIDATION_ENDPOINT
       res = self._Client__send_request(requests.post, url, json=configuration)

       return res.json()


## Using Environment Variables ##
ALOOMA_USERNAME = '__'
ALOOMA_PASSWORD = '__'
ACCOUNT_NAME = 'jumbotail'

event_type = 'dunzo_portal_sales_data'

## We will use this api object throughout the documentation ##
api = ExtClient(ALOOMA_USERNAME, ALOOMA_PASSWORD, account_name=ACCOUNT_NAME)

# Create Consolidation
config = {
   'deployment_name': api.get_deployment_info()['deploymentName'],
   'event_type': event_type,
   'run_at' : '0 0 * * *',
   'query_type':'incremental'
}
api.create_consolidation(config)
