#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 12:44:24 2019

@author: gaurav mahakud
"""

import requests

from alooma import Client

USERNAME = '--' #add username
PASSWORD = '--' #add password
EVENT_TYPE = 'Category_monthly_target'

api = Client(username=USERNAME, password=PASSWORD)
mapping = api.get_mapping(EVENT_TYPE)

mapping['mapping']['tableName']='category_monthly_target_log'
mapping['consolidation']['consolidatedSchema']='public'
mapping['consolidation']['consolidatedTableName']='category_monthly_target'
mapping['consolidation']['consolidationKeys']= ['month']
api.set_mapping(mapping, EVENT_TYPE)